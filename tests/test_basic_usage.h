#pragma once

#include <kaztest/kaztest.h>

#include "jsonic/jsonic.h"

class TestBasicUsage : public TestCase {
public:

    void test_dict_operators() {
        jsonic::Node node(jsonic::NODE_TYPE_DICT);

        node.insert("int_value", 1);
        node.insert("bool_value", jsonic::True);
        node.insert("string_value", "Testing");
        node.insert("none_value", jsonic::None);

        assert_true(1 == node["int_value"]);
        assert_true(jsonic::True == node["bool_value"]);
        assert_true("Testing" == (jsonic::String) node["string_value"]);
        assert_true(jsonic::None == node["none_value"]);
    }

    void test_array_operators() {
        jsonic::Node node(jsonic::NODE_TYPE_LIST);

        node.append(1);
        node.append(jsonic::True);
        node.append("Testing");
        node.append(jsonic::None);

        assert_true(1 == node[0]);
        assert_true(jsonic::True == node[1]);
        assert_true("Testing" == (jsonic::String) node[2]);
        assert_true(jsonic::None == node[3]);
    }

    void test_changing_dict_values() {
        jsonic::Node node(jsonic::NODE_TYPE_DICT);
        node.insert("value", 1); // Insert an integer value
        node["value"] = "Test"; //Change to a string value

        // Check it's a string
        assert_true("Test" == (jsonic::String) node["value"]);
    }

    void test_changing_list_values() {
        jsonic::Node node(jsonic::NODE_TYPE_LIST);
        node.append(1); // Insert an integer value
        node[0] = "Test"; //Change to a string value

        // Check it's a string
        assert_true("Test" == (jsonic::String) node[0]);
    }

};
