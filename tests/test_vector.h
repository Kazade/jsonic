#ifndef TEST_VECTOR_H
#define TEST_VECTOR_H

#include <kaztest/kaztest.h>
#include "jsonic/containers.h"

class TestVector : public TestCase {
public:
    void test_push_back() {
        jsonic::containers::Vector<std::string> vec;
        assert_true(vec.empty());
        assert_equal(0, vec.size());

        vec.push_back("Hello World!");
        assert_equal(1, vec.size());
        assert_false(vec.empty());

        assert_equal("Hello World!", vec[0]);

        vec.push_back("OMGZ");
        assert_equal(2, vec.size());
        assert_equal("Hello World!", vec[0]);
        assert_equal("OMGZ", vec[1]);

        // Sanity check internal state!
        assert_equal(2, vec.reserved_size_);
        assert_equal(2, vec.size_);
    }

    void test_heavy_push_back() {
        jsonic::containers::Vector<std::string> vec;

        for(uint32_t i = 0; i < 100; ++i) {
            vec.push_back("*");
        }

        assert_equal(100, vec.size());
        for(uint32_t i = 0; i < 100; ++i) {
            assert_equal("*", vec[i]);
        }
    }

    void test_pop_back() {
        jsonic::containers::Vector<std::string> vec;
        vec.push_back("1");
        vec.push_back("2");
        vec.pop_back();

        assert_equal(1, vec.size());
        assert_equal(2, vec.reserved_size_);
        assert_equal("1", vec[0]);

        vec.push_back("2");
        vec.push_back("3");
        assert_equal(4, vec.reserved_size_);
        assert_equal(3, vec.size());
    }

    void test_clear() {
        jsonic::containers::Vector<std::string> vec;
        vec.push_back("1");
        vec.push_back("2");
        vec.clear();

        vec.push_back("3");
        assert_equal(1, vec.size());
        assert_equal("3", vec[0]);
    }

    void test_reserve() {
        jsonic::containers::Vector<std::string> vec;
        vec.reserve(100);

        assert_equal(100, vec.reserved_size_);
        assert_equal(0, vec.size());

        vec.push_back("1");
        vec.push_back("2");
        vec.push_back("3");

        assert_equal(100, vec.reserved_size_);
        assert_equal(3, vec.size());
    }
};

#endif // TEST_VECTOR_H

