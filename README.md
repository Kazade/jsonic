# Jsonic - A minimal JSON library for C++

This is a barebones JSON parsing library for C++. It was designed to work on 
embedded platforms and as such it optionally uses its own cut down implementations 
of some STL style containers (vector, unordered_map).

It does not currently support unicode, any unicode implementation would need
to consider space requirements in embedded systems (e.g. Arduino)

For usage, take a look at the deserialization tests: [https://gitlab.com/Kazade/jsonic/blob/master/tests/test_deserialization.h]

The library does not currently support serialization (e.g. dumps()). Patches welcome!