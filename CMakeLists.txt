CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

PROJECT(jsonic)

SET(CMAKE_CXX_FLAGS "-std=c++11 -g")

LINK_LIBRARIES(
    -lrt
)

ADD_LIBRARY(
    jsonic
    SHARED
    ${CMAKE_SOURCE_DIR}/jsonic/jsonic.cpp
)

SET_TARGET_PROPERTIES(
    jsonic
    PROPERTIES
    VERSION 0.0.1
    SOVERSION 1
)

INSTALL(TARGETS jsonic DESTINATION lib)

INSTALL(
    FILES
    ${CMAKE_SOURCE_DIR}/jsonic/jsonic.h
    DESTINATION
    include/jsonic
)

ADD_SUBDIRECTORY(tests)
