#pragma once

#include <kaztest/kaztest.h>

#include "jsonic/containers.h"

class TestBaseString : public TestCase {
public:
    void test_insert() {
        jsonic::containers::BaseString<char> str;

        str.insert(0, "Bananas");
        assert_equal(7, str.length());

        str.insert(2, "na");
        assert_equal(9, str.length());

        assert_true("Banananas" == str);
    }

    void test_find() {
        jsonic::containers::BaseString<char> str;

        str = "Hello World!";

        assert_equal(6, str.find("World"));
        assert_equal(
            jsonic::containers::BaseString<char>::npos,
            str.find("Cheese")
        );
    }

    void test_erase() {
        jsonic::containers::BaseString<char> str;

        str = "Hello World!";

        str.erase(0, 6);

        assert_true(str == "World!");
    }

    void test_push_back() {
        jsonic::containers::BaseString<char> str("test");

        for(char c: std::string("is_connected 0")) {
            str.push_back(c);
        }

        assert_true(str == "testis_connected 0");
    }

    void test_equality() {
        jsonic::containers::BaseString<char> str;

        assert_true(str == "");
        assert_false(str == "a");

        str = "test";

        assert_false(str == "tes");
        assert_false(str == "tests");
        assert_true(str == "test");

        str = "is_connected 0";
        assert_false(str == "1");

        jsonic::containers::BaseString<char> other;

        assert_false(str == other);
        other = "1";
        assert_false(str == other);
        str = "1";
        assert_true(str == other);

        other = "is_connected 0";
        str = jsonic::containers::BaseString<char>(other);
        assert_true("is_connected 0" == str);
        assert_true("is_connected 0" == other);
        assert_true(str == "is_connected 0");
        assert_true(other == "is_connected 0");
        other.push_back('a');
        assert_false(other == "is_connected 0");
        assert_true(other == "is_connected 0a");
    }

    void functional_test() {
        typedef jsonic::containers::BaseString<char> String;
        auto func = []() -> String {
            String result;
            result.push_back('i');
            result.push_back('s');
            result.push_back('_');
            result.push_back('c');
            result.push_back('o');
            result.push_back('n');
            result.push_back('n');
            return result;
        };

        String result = func();
        assert_true(result == "is_conn");
        assert_false(result == "1");
    }
};
